package com.thoughtworks.vapasi.parkinglotsystem.test;
import com.thoughtworks.vapasi.parkinglotsystem.Owner;
import com.thoughtworks.vapasi.parkinglotsystem.ParkingLot;
import com.thoughtworks.vapasi.parkinglotsystem.ParkingStatus;
import com.thoughtworks.vapasi.parkinglotsystem.Vehicle;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class OwnerTest {

    Owner owner = new Owner();

    @Test
    public void ownerShouldBeNotifiedWhenParkingIsFull() {
        Vehicle vehicle = new Vehicle("REGNO1");
        Vehicle vehicle1 = new Vehicle("REGNO2");
        ParkingLot parkingLot = new ParkingLot(2,owner);

        parkingLot.allocateSlot(vehicle);
        parkingLot.allocateSlot(vehicle1);

        assertEquals(ParkingStatus.Full, parkingLot.getOwner().getStatus());
    }

    @Test
    public void ownerShouldBeNotifiedWhenParkingIsFree() {
        Vehicle vehicle = new Vehicle("REGNO1");
        Vehicle vehicle1 = new Vehicle("REGNO2");
        ParkingLot parkingLot = new ParkingLot(2,owner);

        parkingLot.allocateSlot(vehicle);

        assertEquals(ParkingStatus.Free, parkingLot.getOwner().getStatus());
    }
}
