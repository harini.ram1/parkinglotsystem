package com.thoughtworks.vapasi.parkinglotsystem.test;

import com.thoughtworks.vapasi.parkinglotsystem.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class DriverTest {
    Owner owner = new Owner();

    @Test
    public void shouldAllocateParkingSlot() {
        //Arrange
        Vehicle vehicle = new Vehicle("REGNO");
        Driver driver = new Driver("driver1", vehicle);
        ParkingLot parkingLot = new ParkingLot(500,owner);

        //Act
        ParkingToken token = driver.parkVehicle(parkingLot);

        //Assert
        assertNotNull(token);
        assertEquals(vehicle.getVehicleNo(), token.getVehicleNo());
    }

    @Test
    public void shouldUnParkTheVehicle(){

        Vehicle vehicle=new Vehicle("REGNO");
        Driver driver = new Driver("driver",vehicle);
        ParkingLot parkingLot= new ParkingLot(500,owner);

        driver.parkVehicle(parkingLot);

        Vehicle unParkedVehicle = driver.unParkVehicle(parkingLot);
        assertEquals(vehicle,unParkedVehicle);

        assertFalse(driver.hasParkingToken());

    }



}
