package com.thoughtworks.vapasi.parkinglotsystem.test;

import com.thoughtworks.vapasi.parkinglotsystem.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingLotTest {

    Owner owner = new Owner();

    @Test
    public void shouldAllocateParkingSlot() {
        //Arrange
        Vehicle vehicle = new Vehicle("REGNO");
        ParkingLot parkingLot = new ParkingLot(500, owner);

        //Act
        ParkingToken token = parkingLot.allocateSlot(vehicle);

        //Assert
        assertNotNull(token);
        assertEquals(vehicle.getVehicleNo(), token.getVehicleNo());
        assertEquals(499, parkingLot.getCapacity());
    }

    @Test
    public void shouldDeAllocateParkingSlotForAValidToken() throws InvalidTokenException {
        //Arrange
        Vehicle vehicle = new Vehicle("REGNO");
        ParkingLot parkingLot = new ParkingLot(500, owner);

        //Act
        ParkingToken token = parkingLot.allocateSlot(vehicle);
        Vehicle removedVehicle = parkingLot.removeSlot(token,vehicle);

        //Assert
        assertEquals(vehicle.getVehicleNo(),removedVehicle.getVehicleNo());
        assertEquals(token.getVehicleNo(),removedVehicle.getVehicleNo());
        assertEquals(500, parkingLot.getCapacity());
    }

    @Test
    public void shouldNotDeAllocateParkingSlotForAInValidToken() {
        //Arrange
        Vehicle vehicle = new Vehicle("REGNO");
        ParkingLot parkingLot = new ParkingLot(500,owner);
        ParkingToken invalidToken = new ParkingToken("REGNo002");

        //Act
        ParkingToken token = parkingLot.allocateSlot(vehicle);

        //Assert
        assertThrows(InvalidTokenException.class,() -> {parkingLot.removeSlot(invalidToken,vehicle);});
    }

    @Test
    public void ShouldCheckIfParkingIsFull() {
        Vehicle vehicle = new Vehicle("REGNO1");
        Vehicle vehicleTwo = new Vehicle("REGNO2");
        ParkingLot parkingLot = new ParkingLot(2,owner);

        parkingLot.allocateSlot(vehicle);
        parkingLot.allocateSlot(vehicleTwo);

        boolean isParkingFull = parkingLot.isParkingFull();

       assertEquals(true, isParkingFull);
    }

}
