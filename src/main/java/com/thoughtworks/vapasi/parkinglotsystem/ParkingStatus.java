package com.thoughtworks.vapasi.parkinglotsystem;

public enum ParkingStatus {
    Full,
    Free
}
