package com.thoughtworks.vapasi.parkinglotsystem;

public class InvalidTokenException extends Exception {
    public InvalidTokenException(String s)
    {
        super(s);
    }

}
