package com.thoughtworks.vapasi.parkinglotsystem;

public class Owner {
  ParkingStatus status;

  public ParkingStatus notifyMe(ParkingStatus status) {
      this.status = status;
      return status;
    }

  public ParkingStatus getStatus() {
    return status;
  }
}


