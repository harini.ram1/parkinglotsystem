package com.thoughtworks.vapasi.parkinglotsystem;

public class Driver {

    private Vehicle vehicle;
    private String  name;
    private ParkingToken parkingToken;

    public Driver(String name ,Vehicle vehicle) {
        this.vehicle = vehicle;
        this.name = name;
    }

    public ParkingToken parkVehicle(ParkingLot parkingLot) {
        ParkingToken token = parkingLot.allocateSlot(vehicle);
        parkingToken=token;
       return token;
    }

    public Vehicle unParkVehicle(ParkingLot parkingLot) {

        try {
            vehicle = parkingLot.removeSlot(parkingToken,vehicle);
            parkingToken = null;
        } catch (InvalidTokenException e) {
            e.printStackTrace();
        }
        return vehicle;
    }

    public boolean hasParkingToken() {
        return !(parkingToken == null);
    }
}
