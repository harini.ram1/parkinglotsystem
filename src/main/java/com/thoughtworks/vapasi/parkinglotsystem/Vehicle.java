package com.thoughtworks.vapasi.parkinglotsystem;

public class Vehicle {

    private String vehicleNo;

    public Vehicle(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }
}
