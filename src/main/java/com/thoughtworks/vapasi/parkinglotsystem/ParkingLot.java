package com.thoughtworks.vapasi.parkinglotsystem;


public class ParkingLot {

    //private Vehicle vehicle;

    private Owner owner;

    private int capacity ;

    public ParkingLot(int capacity, Owner owner) {
        this.capacity = capacity;
        this.owner = owner;
    }

    public int getCapacity() {
        return capacity;
    }

    public Owner getOwner() {
        return owner;
    }

    public ParkingToken allocateSlot(Vehicle vehicle) {
        this.capacity -= 1;
        notifyOwner();
        ParkingToken token =  new ParkingToken(vehicle.getVehicleNo());
        return token;
    }


    public Vehicle removeSlot(ParkingToken parkingToken,Vehicle vehicle) throws InvalidTokenException {
        this.capacity += 1;
        notifyOwner();
        if(!(vehicle.getVehicleNo().equals(parkingToken.getVehicleNo()))){
            throw new InvalidTokenException("Invalid token provided");

        }
        return vehicle;
    }

    private void notifyOwner() {
        if (isParkingFull()) owner.notifyMe(ParkingStatus.Full);
        else owner.notifyMe(ParkingStatus.Free);
    }

    public boolean isParkingFull() {
        return capacity == 0;
    }

}
